from tornado.web import RequestHandler, Application
from tornado.escape import json_decode, json_encode
from tornado.ioloop import IOLoop
from mongoengine import connect, Document, EmailField, StringField


class User(Document):
    name = StringField()
    email = EmailField()


class UserInsertHandler(RequestHandler):
    def get(self):
        self.finish("""
        <form method="POST">
            <p><label>Name</label><input name="name"/></p>
            <p><label>Email</label><input name="email"/></p>
            <p><input type="submit" /></p>
        </form>""")

    def post(self):
        user = User()
        user.name = self.get_argument("name")
        user.email = self.get_argument("email")
        user = user.save()
        self.redirect("/users/")


class UserHandler(RequestHandler):
    def get(self):
        users = [json_decode(user.to_json()) for user in User.objects]
        self.set_header("Content-Type", "application/json")
        self.finish(json_encode(users))


def main():
    connect("hatch")
    settings = {
        "debug": True,
    }
    app = Application([
        ("/users/insert", UserInsertHandler),
        ("/users/", UserHandler),
    ], **settings)
    app.listen(8080)
    IOLoop.instance().start()


if __name__ == "__main__":
    main()
